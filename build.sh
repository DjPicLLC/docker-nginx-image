#!/bin/bash

# What base image of NGINX to use for all builds
nginx_version=$1

if [[ -v nginx_version ]]; then
  # Create Certificate
  openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout localhost.key -out localhost.crt -config openssl.conf -extensions 'v3_req'
  cp localhost.key static/tls/localhost.key
  cp localhost.crt static/tls/localhost.crt
  mv localhost.key phpfpm/tls/localhost.key
  mv localhost.crt phpfpm/tls/localhost.crt

  # Build standard ngnix image
  cd static || exit
  docker build --build-arg nginx_version=$nginx_version --tag djpic/nginx:$nginx_version-static --tag $CI_REGISTRY_IMAGE/nginx:$nginx_version-static .

  # Build tls NGINX image
  cd tls || exit
  docker build --build-arg nginx_version=$nginx_version --tag djpic/nginx:$nginx_version-static-tls --tag $CI_REGISTRY_IMAGE/nginx:$nginx_version-static-tls .

  # Build phpfpm NGINX image
  cd ../../phpfpm || exit
  docker build --build-arg nginx_version=$nginx_version --tag djpic/nginx:$nginx_version-phpfpm --tag $CI_REGISTRY_IMAGE/nginx:$nginx_version-phpfpm .
  docker tag djpic/nginx:$nginx_version-phpfpm djpic/nginx:latest
  docker tag $CI_REGISTRY_IMAGE/nginx:$nginx_version-phpfpm $CI_REGISTRY_IMAGE/nginx:latest

  # Build phpfpm NGINX image with tls
  cd tls || exit
  docker build --build-arg nginx_version=$nginx_version --tag djpic/nginx:$nginx_version-phpfpm-tls --tag $CI_REGISTRY_IMAGE/nginx:$nginx_version-phpfpm-tls .

  # Push Images
  docker push $CI_REGISTRY_IMAGE/nginx:$nginx_version-static
  docker push $CI_REGISTRY_IMAGE/nginx:$nginx_version-static-tls
  docker push $CI_REGISTRY_IMAGE/nginx:$nginx_version-phpfpm
  docker push $CI_REGISTRY_IMAGE/nginx:$nginx_version-phpfpm-tls
  docker push $CI_REGISTRY_IMAGE/nginx:latest

  docker push djpic/nginx:$nginx_version-static
  docker push djpic/nginx:$nginx_version-static-tls
  docker push djpic/nginx:$nginx_version-phpfpm
  docker push djpic/nginx:$nginx_version-phpfpm-tls
  docker push djpic/nginx:latest


else
  echo "Nginx Version Missing"
  exit 1
fi
