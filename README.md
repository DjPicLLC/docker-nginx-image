# DjPic LLC - NGINX Image

For more information on this repository, visit: [Docker Image: NGINX with PHP FPM](https://www.djpic.net/articles/docker-image-nginx-with-php-fpm/)

Prebuilt image available on DockerHub: [djpic/nginx](https://hub.docker.com/r/djpic/nginx)

Get notified when images are updated via twitter [@djpic_llc](https://twitter.com/djpic_llc)

## Setup Gitlab Runner for Docker
I use a Gitlab runner to build these images.  Directions on setting up the gitlab running Ubuntu is below.
```
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"

dpkg -i gitlab-runner_amd64.deb

gitlab-runner register -n \
--url https://gitlab.com/ \
--registration-token {{TOKEN}} \
--executor docker \
--description "{{RUNNER DESCRIPTION}}" \
--docker-image "docker:dind" \
--docker-privileged \
--docker-volumes "/certs/client"
```